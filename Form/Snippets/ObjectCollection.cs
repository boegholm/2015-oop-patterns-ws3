using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Snippets
{
    class ObjectCollection : IEnumerable<object>, INotifyPropertyChanged
    {
        readonly List<object> _data = new List<object>();

        public void Add(object o)
        {
            _data.Add(o);
            OnPropertyChanged(nameof(Count));
        }

        public int Count => _data.Count;

        #region propertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region IEnumerable implementation
        public IEnumerator<object> GetEnumerator()
        {
            return ((IEnumerable<object>)_data).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion

    }
}