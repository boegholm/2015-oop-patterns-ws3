﻿using System.ComponentModel;

namespace Snippets
{
    class ObjectCollectionPropertyChangedAdapter : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private ObjectCollection _adaptee;
        public ObjectCollectionPropertyChangedAdapter(ObjectCollection adaptee)
        {
            _adaptee = adaptee;
            _adaptee.PropertyChanged += _adaptee_PropertyChanged;
        }

        private void _adaptee_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }

        public int Count => _adaptee.Count;
    }
}