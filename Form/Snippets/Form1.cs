﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Snippets.Annotations;

namespace Snippets
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            label1.DataBindings.Add(new Binding("Text", collection, "Count"));
        }

        ObjectCollection collection = new ObjectCollection();

        private void button1_Click(object sender, EventArgs e)
        {
            collection.Add(new object());
        }
    }
}
