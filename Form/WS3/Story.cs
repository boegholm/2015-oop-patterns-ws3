﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WS3
{
    public class Story : IEnumerable<Story>
    {
        public override string ToString() => Title;

        public string Title { get; }
        public string Content { get; }
        IEnumerable<string> LinesOf(string str)
        {
            string line = null;
            StringReader sr = new StringReader(str);

            while (null != (line = sr.ReadLine()))
            {
                yield return line;
            }
        }

        public Story(string story)
        {
            Title = GetTitle(story);

            var lines = LinesOf(story);
            if (lines.Count() == 1)
            {
                Content = story;
                Title = GetTitle(story);
            }
            else
            {
                foreach (string line in lines)
                {
                    _sections.Add(new Story(line));
                }
            }

        }

        readonly List<Story> _sections = new List<Story>();
        private string GetTitle(string s)
        {
            return s.Substring(0, s.IndexOf(" "));
        }

        public IEnumerator<Story> GetEnumerator()
        {
            return ((IEnumerable<Story>) _sections).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}