﻿using System.ComponentModel;

namespace WS3.Proxies
{
    [ToolboxItem(true)]
    public partial class StoryBodyProxy : TextDisplayProxyBase
    {
        public StoryBodyProxy()
        {
            InitializeComponent();
        }

        protected override void Initialize()
        {
            string text = textDisplay1.Text;
            Controls.Clear();
            textDisplay1 = DisplayFactory?.CreateStoryBody();
            Controls.Add(textDisplay1);
            textDisplay1.Text = text;
        }
    }
}
