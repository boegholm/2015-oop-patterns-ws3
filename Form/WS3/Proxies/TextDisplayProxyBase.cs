﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using WS3.ArialText;

namespace WS3.Proxies
{
    [ToolboxItem(false)]
    public partial class TextDisplayProxyBase : UserControl
    {
        private static AbstractDisplayFactory _displayFactory = new BoringTextFactory();
        public static event Action FactoryChanged;
        [Browsable(false)]
        [ReadOnly(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public static AbstractDisplayFactory DisplayFactory
        {
            get { return _displayFactory; }
            set
            {
                _displayFactory = value;
                FactoryChanged?.Invoke();
            }
        }
        public TextDisplayProxyBase()
        {
            InitializeComponent();
            FactoryChanged += ContainerBase_FactoryChanged;
            if (!DesignMode)
                Initialize();
        }

        private void ContainerBase_FactoryChanged()
        {
            if (!DesignMode)
                Initialize();
        }

        protected virtual void Initialize()
        {
            //string text = bigHeadline1.Text;
            //Controls.Clear();
            //bigHeadline1 = DisplayFactory?.CreateBigHeadline();
            //Controls.Add(bigHeadline1);
            //bigHeadline1.Text = text;
        }
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public override string Text
        {
            get
            {
                if (textDisplay1 != null)
                    return textDisplay1?.Text;
                else return Name;
            }
            set
            {
                if (textDisplay1 != null)
                    textDisplay1.Text = value;
            }
        }

    }
}
