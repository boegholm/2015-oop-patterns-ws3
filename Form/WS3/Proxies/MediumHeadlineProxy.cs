﻿using System.ComponentModel;

namespace WS3.Proxies
{
    [ToolboxItem(true)]
    public partial class MediumHeadlineProxy : TextDisplayProxyBase
    {
        public MediumHeadlineProxy()
        {
            InitializeComponent();
        }
        protected override void Initialize()
        {
            string text = textDisplay1.Text;
            Controls.Clear();
            textDisplay1 = DisplayFactory?.CreateMediumHeadline();
            Controls.Add(textDisplay1);
            textDisplay1.Text = text;
        }
    }
}
