﻿namespace WS3.Proxies
{
    partial class MediumHeadlineProxy
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // textDisplay1
            // 
            this.textDisplay1.Font = new System.Drawing.Font("Ravie", 32F);
            this.textDisplay1.Margin = new System.Windows.Forms.Padding(0);
            // 
            // MediumHeadlineProxy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Name = "MediumHeadlineProxy";
            this.Size = new System.Drawing.Size(394, 60);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
