using System.Windows.Forms;

namespace WS3.StandardTextDisplay
{
    public class StandardTextFactory : AbstractDisplayFactory
    {
        public override UserControl CreateBigHeadline()
        {
            return new BigHeadline();
        }

        public override UserControl CreateMediumHeadline()
        {
            return new MediumHeadline();
        }


        public override UserControl CreateStoryBody()
        {
            return new StoryBody();
        }
    }
}