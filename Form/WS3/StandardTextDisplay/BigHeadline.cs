﻿using System.ComponentModel;
using System.Drawing;
using WS3.UI;

namespace WS3.StandardTextDisplay
{
    [ToolboxItem(true)]
    public partial class BigHeadline : TextDisplay
    {
        public BigHeadline()
        {
            InitializeComponent();
            FontSize = 48.0f;
        }

        bool _fontset = false; // workaround for VS designer
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override Font Font
        {
            get
            {
                if (!_fontset)
                    return _defaultFont;
                return (base.Font);
            }
            set
            {
                _fontset = true;
                base.Font = value;
                OnPropertyChanged();
            }
        }
    }
}
