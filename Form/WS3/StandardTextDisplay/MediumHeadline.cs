﻿using System.ComponentModel;
using WS3.UI;

namespace WS3.StandardTextDisplay
{
    [ToolboxItem(true)]
    public class MediumHeadline : TextDisplay
    {
        public MediumHeadline()
        {
            FontSize = 32.0f;
        }
    }
}