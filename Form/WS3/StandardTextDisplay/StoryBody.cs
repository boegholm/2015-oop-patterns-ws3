﻿using System.ComponentModel;
using WS3.UI;

namespace WS3.StandardTextDisplay
{
    [ToolboxItem(true)]
    public partial class StoryBody : TextDisplay
    {
        public StoryBody()
        {
            InitializeComponent();
            this.lbl_content.MaximumSize = new System.Drawing.Size(500, 0);
        }
    }
}
