﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WS3.StandardTextDisplay;

namespace WS3.VeryFancyTextDisplay
{
    public partial class VeryFancyBigHeadline : BigHeadline
    {
        public VeryFancyBigHeadline()
        {
            InitializeComponent();
        }

        int _colorIndex = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            KnownColor[] values = (KnownColor[])Enum.GetValues(typeof(KnownColor));
            this._colorIndex = (this._colorIndex + 1) %values.Length;
            KnownColor selected = values[this._colorIndex];
            lbl_content.ForeColor = Color.FromKnownColor(selected);
        }
    }
}
