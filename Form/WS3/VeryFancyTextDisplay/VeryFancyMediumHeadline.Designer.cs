﻿namespace WS3.VeryFancyTextDisplay
{
    partial class VeryFancyMediumHeadline
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // lbl_content
            // 
            this.lbl_content.Font = new System.Drawing.Font("Palatino Linotype", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_content.Size = new System.Drawing.Size(145, 58);
            // 
            // VeryFancyMediumHeadline
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(36F, 57F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "VeryFancyMediumHeadline";
            this.Size = new System.Drawing.Size(145, 58);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
