﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WS3.Standard;

namespace WS3.VeryFancyTextDisplay
{
    class FancyTextDisplayFactory : AbstractDisplayFactory
    {
        public override UserControl CreateBigHeadline()
        {
            return new VeryFancyBigHeadline();
        }

        public override UserControl CreateMediumHeadline()
        {
            return new VeryFancyMediumHeadline();
        }

        public override UserControl CreateStoryBody()
        {
            return new VeryFancyStoryBody();
        }
    }
}
