﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WS3.StandardTextDisplay;

namespace WS3.VeryFancyTextDisplay
{
    public partial class VeryFancyMediumHeadline : MediumHeadline
    {
        public VeryFancyMediumHeadline()
        {
            InitializeComponent();
        }

        string text;
        public override string Text
        {
            get
            {
                if (text == null)
                    return lbl_content.Text;
                else return text;
            }
            set
            {
                text = value;
                lbl_content.Text = $"[[ {value} ]]";
            }
        }
    }
}
