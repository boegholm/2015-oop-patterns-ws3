﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WS3.Standard;
using WS3.StandardTextDisplay;

namespace WS3.VeryFancyTextDisplay
{
    public partial class VeryFancyStoryBody : StoryBody
    {
        public VeryFancyStoryBody()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            DialogResult dr = cd.ShowDialog(this);
            if (dr == DialogResult.OK)
            {
                BackColor = cd.Color;
            }
        }
    }
}
