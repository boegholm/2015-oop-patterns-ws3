﻿using System.ComponentModel;
using WS3.StandardTextDisplay;

namespace WS3.BoringTextDisplay
{
    public partial class ArialStoryBody : StoryBody
    {
        public ArialStoryBody()
        {
            InitializeComponent();
            this.lbl_content.MaximumSize = new System.Drawing.Size(500, 0);
        }
    }
}
