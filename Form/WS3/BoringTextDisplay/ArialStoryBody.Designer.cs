﻿namespace WS3.BoringTextDisplay
{
    partial class ArialStoryBody
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // lbl_content
            // 
            this.lbl_content.Font = new System.Drawing.Font("Arial", 10F);
            this.lbl_content.Size = new System.Drawing.Size(46, 16);
            // 
            // StoryBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Name = "StoryBody";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
