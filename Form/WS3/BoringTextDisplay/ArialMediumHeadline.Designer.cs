﻿namespace WS3.BoringTextDisplay
{
    partial class ArialMediumHeadline
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // lbl_content
            // 
            this.lbl_content.Font = new System.Drawing.Font("Arial", 32F);
            this.lbl_content.Size = new System.Drawing.Size(138, 49);
            // 
            // ArialBigHeadline
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(53F, 86F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ArialBigHeadline";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
    }
}
