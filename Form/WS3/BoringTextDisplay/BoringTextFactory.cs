using System.Windows.Forms;
using WS3.BoringTextDisplay;
using WS3.Standard;

namespace WS3.ArialText
{
    public class BoringTextFactory : AbstractDisplayFactory
    {
        public override UserControl CreateBigHeadline()
        {
            return new ArialBigHeadline();
        }

        public override UserControl CreateMediumHeadline()
        {
            return new ArialMediumHeadline();
        }


        public override UserControl CreateStoryBody()
        {
            return new ArialStoryBody();
        }
    }
}