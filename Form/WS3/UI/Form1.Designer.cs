﻿using WS3.Proxies;

namespace WS3.UI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.mediumHeadlineContainer4 = new WS3.Proxies.MediumHeadlineProxy();
            this.mediumHeadlineContainer3 = new WS3.Proxies.MediumHeadlineProxy();
            this.mediumHeadlineContainer2 = new WS3.Proxies.MediumHeadlineProxy();
            this.mediumHeadlineContainer1 = new WS3.Proxies.MediumHeadlineProxy();
            this.bigHeadlineContainer1 = new WS3.Proxies.BigHeadlineProxy();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(827, 208);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 49);
            this.button1.TabIndex = 5;
            this.button1.Text = "Read";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.button2.Location = new System.Drawing.Point(827, 271);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 49);
            this.button2.TabIndex = 11;
            this.button2.Text = "Read";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.button3.Location = new System.Drawing.Point(827, 344);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 49);
            this.button3.TabIndex = 12;
            this.button3.Text = "Read";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold);
            this.button7.Location = new System.Drawing.Point(827, 418);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 49);
            this.button7.TabIndex = 33;
            this.button7.Text = "Read";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // mediumHeadlineContainer4
            // 
            this.mediumHeadlineContainer4.AutoSize = true;
            this.mediumHeadlineContainer4.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mediumHeadlineContainer4.Location = new System.Drawing.Point(41, 418);
            this.mediumHeadlineContainer4.Name = "mediumHeadlineContainer4";
            this.mediumHeadlineContainer4.Size = new System.Drawing.Size(139, 49);
            this.mediumHeadlineContainer4.TabIndex = 32;
            this.mediumHeadlineContainer4.Text = "story4";
            // 
            // mediumHeadlineContainer3
            // 
            this.mediumHeadlineContainer3.AutoSize = true;
            this.mediumHeadlineContainer3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mediumHeadlineContainer3.Location = new System.Drawing.Point(41, 344);
            this.mediumHeadlineContainer3.Name = "mediumHeadlineContainer3";
            this.mediumHeadlineContainer3.Size = new System.Drawing.Size(139, 49);
            this.mediumHeadlineContainer3.TabIndex = 30;
            this.mediumHeadlineContainer3.Text = "story3";
            // 
            // mediumHeadlineContainer2
            // 
            this.mediumHeadlineContainer2.AutoSize = true;
            this.mediumHeadlineContainer2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mediumHeadlineContainer2.Location = new System.Drawing.Point(41, 271);
            this.mediumHeadlineContainer2.Name = "mediumHeadlineContainer2";
            this.mediumHeadlineContainer2.Size = new System.Drawing.Size(139, 49);
            this.mediumHeadlineContainer2.TabIndex = 29;
            this.mediumHeadlineContainer2.Text = "story2";
            // 
            // mediumHeadlineContainer1
            // 
            this.mediumHeadlineContainer1.AutoSize = true;
            this.mediumHeadlineContainer1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mediumHeadlineContainer1.Location = new System.Drawing.Point(41, 208);
            this.mediumHeadlineContainer1.Name = "mediumHeadlineContainer1";
            this.mediumHeadlineContainer1.Size = new System.Drawing.Size(139, 49);
            this.mediumHeadlineContainer1.TabIndex = 26;
            this.mediumHeadlineContainer1.Text = "story1";
            // 
            // bigHeadlineContainer1
            // 
            this.bigHeadlineContainer1.AutoSize = true;
            this.bigHeadlineContainer1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.bigHeadlineContainer1.Location = new System.Drawing.Point(282, 9);
            this.bigHeadlineContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.bigHeadlineContainer1.MinimumSize = new System.Drawing.Size(20, 20);
            this.bigHeadlineContainer1.Name = "bigHeadlineContainer1";
            this.bigHeadlineContainer1.Size = new System.Drawing.Size(371, 92);
            this.bigHeadlineContainer1.TabIndex = 17;
            this.bigHeadlineContainer1.Text = "Story Book";
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(12, 9);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(201, 40);
            this.button6.TabIndex = 31;
            this.button6.Text = "Boring style";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(12, 55);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(201, 37);
            this.button5.TabIndex = 22;
            this.button5.Text = "Fancy style";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(12, 98);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(201, 38);
            this.button4.TabIndex = 21;
            this.button4.Text = "Very fancy style";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1033, 755);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.mediumHeadlineContainer4);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.mediumHeadlineContainer3);
            this.Controls.Add(this.mediumHeadlineContainer2);
            this.Controls.Add(this.mediumHeadlineContainer1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.bigHeadlineContainer1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private BigHeadlineProxy bigHeadlineContainer1;
        private MediumHeadlineProxy mediumHeadlineContainer1;
        private MediumHeadlineProxy mediumHeadlineContainer2;
        private MediumHeadlineProxy mediumHeadlineContainer3;
        private MediumHeadlineProxy mediumHeadlineContainer4;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
    }
}

