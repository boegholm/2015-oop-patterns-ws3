﻿using System.ComponentModel;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using WS3.Annotations;

namespace WS3.UI
{
    [ToolboxItem(false)]
    public partial class TextDisplay : UserControl, INotifyPropertyChanged
    {
        protected virtual Font _defaultFont { get; } = new Font("Ravie", 10.0f);
        public TextDisplay()
        {
            InitializeComponent();
            lbl_content.TextChanged += (sender, args) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Text)));
            AutoSize = true;
            TabStop = false;
            AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
        }

        // så kan vi sætte font og størrelse hver for sig
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public string FontFamily
        {
            get { return Font.FontFamily.Name; }
            set { Font = new Font(value, FontSize); }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual float FontSize
        {
            get { return Font.Size; }
            set { Font = new Font(FontFamily, value); }
        }


        bool _textSet = false; // vs-designer buggede
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public override string Text
        {
            get
            {
                if (!_textSet)
                    return Name;
                return lbl_content.Text;
            }
            set
            {
                lbl_content.Text = value;
                _textSet = true;
            }
        }

        bool _fontset = false; // workaround for VS designer
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public override Font Font
        {
            get
            {
                if (!_fontset)
                    return _defaultFont;
                return (base.Font);
            }
            set
            {
                _fontset = true;
                base.Font = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
