﻿using System;
using System.Windows.Forms;
using WS3.ArialText;
using WS3.Proxies;
using WS3.Standard;
using WS3.StandardTextDisplay;
using WS3.VeryFancyTextDisplay;

namespace WS3.UI
{
    public partial class Form1 : Form
    {
        private Stories Stories { get; } = new Stories();

        public Form1()
        {
            InitializeComponent();

            mediumHeadlineContainer1.Text = Stories.CupcakeStory.Title+"-story";
            mediumHeadlineContainer2.Text = Stories.CheeseStory.Title + "-story";
            mediumHeadlineContainer3.Text = Stories.BaconStory.Title + "-story";
            mediumHeadlineContainer4.Text = Stories.LoremStory.Title + "-story";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DisplayStory(Stories.CupcakeStory);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DisplayStory(Stories.CheeseStory);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DisplayStory(Stories.BaconStory);
        }

        private void DisplayStory(Story story)
        {
            StoryDisplay sd = new StoryDisplay
            {
                Story = story,
                StartPosition = FormStartPosition.CenterScreen
            };
//            sd.ShowDialog(this);
            sd.Show(); // allow us to change factories!
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            TextDisplayProxyBase.DisplayFactory = new FancyTextDisplayFactory();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            TextDisplayProxyBase.DisplayFactory = new StandardTextFactory();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            TextDisplayProxyBase.DisplayFactory = new BoringTextFactory();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            DisplayStory(Stories.LoremStory);
        }
    }
}
