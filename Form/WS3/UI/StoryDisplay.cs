﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WS3.Annotations;
using WS3.Proxies;

namespace WS3.Standard
{
    internal interface IStoryDisplayBuilder
    {
        Form GetForm();
        void AddBigHeadline(string headline);
        void AddMediumHeadline(string headline);
        void AddStoryText(string text);
        void NewSection();
    }

    class StoryDisplayBuilder : IStoryDisplayBuilder
    {
        Form product = new Form();
        public StoryDisplayBuilder(){}
        public StoryDisplayBuilder(Form existingForm)
        {
            product = existingForm;
            product.Controls.Clear();
        }
        public Form GetForm() => product;

        int _position = 7;
        public void AddBigHeadline(string headline)
        {
            BigHeadlineProxy bh = new BigHeadlineProxy();
            bh.Text = $"{headline}-story";
            product.Controls.Add(bh);
            bh.Margin = new Padding(0);
            int xpos = (product.Width - bh.Width) / 2;
            bh.Location = new Point(xpos, _position);
            _position += bh.Height + 10;
        }
        public void AddMediumHeadline(string headline)
        {
            MediumHeadlineProxy mh = new MediumHeadlineProxy();
            mh.Text = headline;
            product.Controls.Add(mh);
            mh.Location = new Point(30, _position);
            _position += mh.Height;
        }
        public void AddStoryText(string text)
        {
            StoryBodyProxy td = new StoryBodyProxy();
            td.Text = text;
            product.Controls.Add(td);
            td.Location = new Point(90, _position);
            _position += td.Height;
        }
        public void NewSection()
        {
            _position += 6;
        }
    }



    public partial class StoryDisplay : Form, INotifyPropertyChanged
    {
        private Story _story;

        public StoryDisplay()
        {
            InitializeComponent();
            PropertyChanged += StoryDisplay_PropertyChanged;
            TextDisplayProxyBase.FactoryChanged += GenerateView;
        }

        private void NewGenerate()
        {
            // generate display
            if (Story != null)
            {
                IStoryDisplayBuilder sb = new StoryDisplayBuilder(this);
                sb.AddBigHeadline($"{Story.Title}-story");
                foreach (var paragraph in Story)
                {
                    sb.AddMediumHeadline(paragraph.Title);
                    sb.AddStoryText(paragraph.Content);
                    sb.NewSection();
                }
            }
        }

        private void GenerateView()
        {
            // generate display
            if (Story != null)
            {
                Controls.Clear();
                BigHeadlineProxy bh = new BigHeadlineProxy();
                bh.Text = $"{Story.Title}-story";
                Controls.Add(bh);
                bh.Margin = new Padding(0);
                int xpos = (Width - bh.Width) / 2;
                bh.Location = new Point(xpos, 7);

                int position = bh.Location.Y + bh.Height + 10;
                foreach (var paragraph in Story)
                {
                    MediumHeadlineProxy mh = new MediumHeadlineProxy();
                    mh.Text = paragraph.Title;
                    Controls.Add(mh);
                    mh.Location = new Point(30, position);
                    position += mh.Height;
                    StoryBodyProxy td = new StoryBodyProxy();
                    td.Text = paragraph.Content;
                    Console.WriteLine(paragraph.Content);
                    Controls.Add(td);
                    td.Location = new Point(90, position);
                    position += td.Height;
                    position += 6;
                }
            }
        }

        private void StoryDisplay_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(Story))
            {
                GenerateView();
            }
        }

        public Story Story
        {
            get { return _story; }
            set
            {
                if (Equals(value, _story)) return;
                _story = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void StoryDisplay_ResizeEnd(object sender, EventArgs e)
        {
            GenerateView();
        }
    }
}
