﻿using System.Windows.Forms;

namespace WS3
{
    public abstract class AbstractDisplayFactory
    {
        public abstract UserControl CreateBigHeadline();
        public abstract UserControl CreateMediumHeadline();
        public abstract UserControl CreateStoryBody();
    }
}